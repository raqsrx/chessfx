package lk.raqeeb.chessfx.util;

public class BoardConstants {
    public static final int BOARD_WIDTH = 8;
    public static final int BOARD_HEIGHT = 8;
    public static final int TILE_SIZE = 100;
}
