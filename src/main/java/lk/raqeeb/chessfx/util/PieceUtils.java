package lk.raqeeb.chessfx.util;

import lk.raqeeb.chessfx.model.PieceType;

public class PieceUtils {
    public static PieceType getPieceTypeByLocation(int x, int y) {
        if (y == 1 || y == 6) {
            return PieceType.PAWN;
        }
        for (PieceType type : PieceType.values()) {
            if (x == type.getX() || (x > 4 && x == (7 - type.getX()))) {
                return type;
            }
        }
        return null;
    }
}
