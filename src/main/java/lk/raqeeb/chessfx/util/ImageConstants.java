package lk.raqeeb.chessfx.util;

public class ImageConstants {
    public static final String KING = "src\\main\\resources\\images\\%s_king.png";
    public static final String QUEEN = "src\\main\\resources\\images\\%s_queen.png";
    public static final String BISHOP = "src\\main\\resources\\images\\%s_bishop.png";
    public static final String ROOK = "src\\main\\resources\\images\\%s_rook.png";
    public static final String KNIGHT = "src\\main\\resources\\images\\%s_knight.png";
    public static final String PAWN = "src\\main\\resources\\images\\%s_pawn.png";
    public static final int WIDTH = 60;
    public static final int HEIGHT = 60;
    public static final int TRANSLATE_X = 20;
    public static final int TRANSLATE_Y = 20;
}
