package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class KnightPieceMover extends BasePieceMover {
    public KnightPieceMover(Tile tile) {
        super(tile);
    }

    /**
     * Get valid navigable tile coordinates for Knight
     * The Knight moves in a L pattern. i.e: [(x +- 2) (y +- 1)], [(x +- 1) (y +- 2)]
     * The BasePieceMover class will automatically filter out invalid tile coordinates
     *
     * @return - List of Point instances with valid navigable tile coordinates
     */
    @Override
    public List<Point> getMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        int oldX = this.piece.getLocation().x / BoardConstants.TILE_SIZE;
        int oldY = this.piece.getLocation().y / BoardConstants.TILE_SIZE;

        movablePoints.add(new Point(oldX + 1, oldY + 2));
        movablePoints.add(new Point(oldX + 1, oldY - 2));
        movablePoints.add(new Point(oldX - 1, oldY + 2));
        movablePoints.add(new Point(oldX - 1, oldY - 2));
        movablePoints.add(new Point(oldX + 2, oldY - 1));
        movablePoints.add(new Point(oldX + 2, oldY + 1));
        movablePoints.add(new Point(oldX - 2, oldY + 1));
        movablePoints.add(new Point(oldX - 2, oldY - 1));

        return movablePoints;
    }
}
