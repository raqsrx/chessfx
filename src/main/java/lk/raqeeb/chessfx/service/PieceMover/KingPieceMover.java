package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class KingPieceMover extends BasePieceMover {
    public KingPieceMover(Tile tile) {
        super(tile);
    }

    /**
     * Get navigable tile coordinates for King Piece
     * King can move one tile to any direction (horizontally, vertically or diagonally)
     * The BasePieceMover will automatically filter out invalid tile coordinates
     *
     * @return - List of Point instances containing the navigable Tile coordinates
     */
    @Override
    public List<Point> getMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        int oldX = piece.getLocation().x / BoardConstants.TILE_SIZE;
        int oldY = piece.getLocation().y / BoardConstants.TILE_SIZE;

        movablePoints.add(new Point(oldX, oldY + 1));
        movablePoints.add(new Point(oldX, oldY - 1));
        movablePoints.add(new Point(oldX - 1, oldY));
        movablePoints.add(new Point(oldX - 1, oldY + 1));
        movablePoints.add(new Point(oldX - 1, oldY - 1));
        movablePoints.add(new Point(oldX + 1, oldY));
        movablePoints.add(new Point(oldX + 1, oldY + 1));
        movablePoints.add(new Point(oldX + 1, oldY - 1));

        return movablePoints;
    }
}
