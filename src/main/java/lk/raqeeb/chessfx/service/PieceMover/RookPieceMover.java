package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class RookPieceMover extends BasePieceMover {
    private int currentX;
    private int currentY;

    public RookPieceMover(Tile tile) {
        super(tile);
    }

    /**
     * Gets the valid navigable Tiles that the Rook can move to
     * Gets the horizontal and vertical Points that the Rook can move to
     *
     * @return - List of Point instances containing the valid navigable Tile coordinates
     */
    @Override
    public List<Point> getMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        this.currentX = this.piece.getLocation().x / BoardConstants.TILE_SIZE;
        this.currentY = this.piece.getLocation().y / BoardConstants.TILE_SIZE;

        for (int y = getMinY(); y <= getMaxY(); y++) {
            movablePoints.add(new Point(currentX, y));
        }
        for (int x = getMinX(); x <= getMaxX(); x++) {
            movablePoints.add(new Point(x, currentY));
        }
        return movablePoints;
    }

    /**
     * Gets the max Y coordinate that the Rook can move to from its current Position
     * If a Piece from the same side exists, the last unoccupied Tile before the Piece is returned
     * If a Piece from the opposite side exists, the coordinates of the occupied Piece is returned
     *
     * @return - The max navigable Y coordinate
     */
    private int getMaxY() {
        for (int y = currentY + 1; y < BoardConstants.BOARD_HEIGHT; y++) {
            if (boardTiles[currentX][y].hasPiece()) {
                return isOppositeSide(boardTiles[currentX][y]) ? y : y - 1;
            }
        }
        return BoardConstants.BOARD_HEIGHT - 1;
    }

    /**
     * Gets the minimum Y coordinate that the Rook can move to
     * If a Piece from the same side exists, the unoccupied Tile before that is returned
     * If a Piece from the opposite side exists, the Tile occupied by the Piece is returned
     *
     * @return - The minimum navigable Y coordinate
     */
    private int getMinY() {
        for (int y = currentY - 1; y > -1; y--) {
            if (boardTiles[currentX][y].hasPiece()) {
                return isOppositeSide(boardTiles[currentX][y]) ? y : y + 1;
            }
        }
        return 0;
    }

    /**
     * Gets the max X coordinate that the Rook can move to from its current Position
     * If a Piece from the same side exists, the last unoccupied Tile before the Piece is returned
     * If a Piece from the opposite side exists, the coordinates of the occupied Piece is returned
     *
     * @return - The max navigable X coordinate
     */
    private int getMaxX() {
        for (int x = currentX + 1; x < BoardConstants.BOARD_WIDTH; x++) {
            if (boardTiles[x][currentY].hasPiece()) {
                return isOppositeSide(boardTiles[x][currentY]) ? x : x - 1;
            }
        }
        return BoardConstants.BOARD_WIDTH - 1;
    }

    /**
     * Gets the minimum X coordinate that the Rook can move to
     * If a Piece from the same side exists, the unoccupied Tile before that is returned
     * If a Piece from the opposite side exists, the Tile occupied by the Piece is returned
     *
     * @return - The minimum navigable X coordinate
     */
    private int getMinX() {
        for (int x = currentX - 1; x > -1; x--) {
            if (boardTiles[x][currentY].hasPiece()) {
                return isOppositeSide(boardTiles[x][currentY]) ? x : x + 1;
            }
        }
        return 0;
    }
}
