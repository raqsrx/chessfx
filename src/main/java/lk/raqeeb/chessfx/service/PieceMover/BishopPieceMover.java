package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BishopPieceMover extends BasePieceMover {
    private int currentX;
    private int currentY;

    public BishopPieceMover(Tile tile) {
        super(tile);
    }

    /**
     * Get valid navigable coordinates that the Bishop can move to
     * Get the right and left diagonal points that the bishop can move to
     *
     * @return - List of Point instances containing the valid coordinates
     */
    @Override
    public List<Point> getMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        this.currentX = piece.getLocation().x / BoardConstants.TILE_SIZE;
        this.currentY = piece.getLocation().y / BoardConstants.TILE_SIZE;

        Point rightDiagonalMaxPoint = getRightDiagonalMaxPoint();
        Point rightDiagonalMinPoint = getRightDiagonalMinPoint();
        Point leftDiagonalMaxPoint = getLeftDiagonalMaxPoint();
        Point leftDiagonalMinPoint = getLeftDiagonalMinPoint();

        for (int y = rightDiagonalMinPoint.y, x = rightDiagonalMinPoint.x;
             y <= rightDiagonalMaxPoint.y && x >= rightDiagonalMaxPoint.x;
             y++, x--
        ) {
            movablePoints.add(new Point(x, y));
        }

        for (int y = leftDiagonalMinPoint.y, x = leftDiagonalMinPoint.x;
             y <= leftDiagonalMaxPoint.y && x <= leftDiagonalMaxPoint.x;
             y++, x++
        ) {
            movablePoints.add(new Point(x, y));
        }

        return movablePoints;
    }

    /**
     * Gets the max left diagonal coordinates where the Bishop can move to from its current position
     * The left diagonal max point indicates the point with the highest Y value
     *
     * @return - Point containing the coordinates of the left diagonal max point
     */
    private Point getLeftDiagonalMaxPoint() {
        int y = currentY + 1;
        int x = currentX + 1;

        for (; y < BoardConstants.BOARD_HEIGHT && x < BoardConstants.BOARD_WIDTH; y++, x++) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }
        return new Point(x - 1, y - 1);
    }

    /**
     * Gets the left diagonal min point coordinates that the Bishop can move to
     * The left diagonal min point indicates the point with the lowest Y value
     *
     * @return - Point instance containing the left diagonal min coordinates
     */
    private Point getLeftDiagonalMinPoint() {
        int y = currentY - 1;
        int x = currentX - 1;

        for (; y > -1 && x > -1; y--, x--) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }

        return new Point(x + 1, y + 1);
    }

    /**
     * Gets the right diagonal max point coordinates
     * The right diagonal max point indicates the point in the right diagonal direction with the highest Y value
     *
     * @return - Point instance containing the right diagonal max point coordinates
     */
    private Point getRightDiagonalMaxPoint() {
        int y = currentY + 1;
        int x = currentX - 1;

        for (; y < BoardConstants.BOARD_HEIGHT && x > -1; y++, x--) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }
        return new Point(x + 1, y - 1);
    }

    /**
     * Gets the right diagonal min point coordinates
     * THe right diagonal min point indicates the point in the right diagonal with the lowest Y value
     *
     * @return - Point instance containing the right diagonal min point coordinates
     */
    private Point getRightDiagonalMinPoint() {
        int y = currentY - 1;
        int x = currentX + 1;

        for (; y > -1 && x < BoardConstants.BOARD_WIDTH; y--, x++) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }
        return new Point(x - 1, y + 1);
    }
}
