package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Piece;
import lk.raqeeb.chessfx.model.PieceType;
import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.service.PiecePromoter.FxPiecePromoter;
import lk.raqeeb.chessfx.servicefacade.PieceMover;
import lk.raqeeb.chessfx.servicefacade.PiecePromoter;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public abstract class BasePieceMover implements PieceMover {
    private List<Tile> movableTiles;
    protected Tile tile;
    protected Piece piece;
    protected Tile[][] boardTiles;

    public BasePieceMover(Tile tile) {
        this.tile = tile;
        this.piece = tile.getPiece();
        this.movableTiles = new ArrayList<>();
    }

    /**
     * Set board tiles to enable traversing through the tiles
     *
     * @param boardTiles - The 2D array containing the Tile instances
     */
    @Override
    public final void setBoardTiles(Tile[][] boardTiles) {
        this.boardTiles = boardTiles;
        setPieceSelectedEventListeners();
    }

    /**
     * Moves the selected Piece to the passed Tile
     * If passed Tile contains a Piece of the opposite side, captures it
     *
     * @param tile - The Tile instance to which the Piece has to be set
     */
    @Override
    public final void tryMove(Tile tile) {
        if (!tile.isSelected()) {
            return;
        }
        if (tile.hasPiece()) {
            tryKill(tile.getPiece());
        }
        piece.setLocation(tile.getXPos(), tile.getYPos());
        transferPieceToTile(tile);
        setMovableTiles(getMovablePoints());
        clearHighlightedTiles();
        if (piece.getType() == PieceType.PAWN && (tile.getYPos() == 0 || tile.getYPos() == 7)) {
            tryPromote();
        }
    }

    /**
     * Gets the invalid Tile instances where the side of the piece is the same as the selected piece
     *
     * @return - List of Point instances containing the coordinates of the invalid Tiles
     */
    public final List<Point> getTilesWithPieces() {
        List<Point> points = new ArrayList<>();
        for (int x = 0; x < BoardConstants.BOARD_WIDTH; x++) {
            for (int y = 0; y < BoardConstants.BOARD_HEIGHT; y++) {
                if (boardTiles[x][y].hasPiece() && boardTiles[x][y].getPiece().getSide() == this.piece.getSide()) {
                    points.add(new Point(x, y));
                }
            }
        }
        return points;
    }

    /**
     * Sets the coordinates of valid Tiles that the selected Piece can move to
     *
     * @param movablePoints - List of Point instances with the coordinates of the valid Tile instances
     */
    @Override
    public final void setMovableTiles(List<Point> movablePoints) {
        movableTiles.clear();
        movablePoints.stream()
                .filter(point -> getTilesWithPieces().stream()
                        .noneMatch(invalidPoint -> invalidPoint.x == point.x && invalidPoint.y == point.y))
                .filter(point -> point.x >= 0 && point.x < 8 && point.y >= 0 && point.y < 8)
                .forEach(point -> movableTiles.add(boardTiles[point.x][point.y]));
    }

    /**
     * Captures / Kills the passed Piece instance thereby putting it out of the match
     *
     * @param piece - The Piece instance which will be captured
     */
    @Override
    public final void tryKill(Piece piece) {
        piece.setAlive(false);
    }

    @Override
    public final void tryPromote() {
        PiecePromoter piecePromoter = new FxPiecePromoter(piece.getSide());
        PieceType type = piecePromoter.getTargetPieceType();
        piece.setType(type);
        piece.setPieceMover(this.tile);
        piece.getPieceMover().setBoardTiles(boardTiles);
    }

    /**
     * Checks if the passed Tile instance contains a Piece belonging to the opposite side
     *
     * @param tile - The Tile instance containing the Piece
     * @return - True if the Tile contains a Piece of the opposite side, False otherwise
     */
    protected final boolean isOppositeSide(Tile tile) {
        return tile.hasPiece() && tile.getPiece().getSide() != this.piece.getSide();
    }

    /**
     * Sets the Mouse click event listeners for the Piece and the relevant navigable Tiles
     * Applies / Removes highlighting based on the click event for the Piece
     * Invokes tryMove when a navigable tile is chosen for the Piece
     */
    private void setPieceSelectedEventListeners() {
        piece.setOnMouseClicked(event -> {
            if (piece.isSelected()) {
                clearHighlightedTiles();
                piece.setSelected(false);
                return;
            }
            clearHighlightedTiles();
            setMovableTiles(getMovablePoints());
            piece.setSelected(true);
            movableTiles.forEach(movableTile -> {
                movableTile.selectTile();
                movableTile.setOnMouseClicked(tEvent -> {
                    tryMove(movableTile);
                    piece.setSelected(false);
                });
            });
        });
    }

    /**
     * Deselects the highlighted Tile instances
     */
    private void clearHighlightedTiles() {
        Stream.of(boardTiles)
                .flatMap(Arrays::stream)
                .filter(Tile::isSelected)
                .forEach(Tile::selectTile);
    }

    /**
     * Transfers the moved Piece to the relevant selected Tile
     * Assigns the selected Tile to the property tile for future operations
     *
     * @param tile - The selected Tile instance to where the Piece will be moved
     */
    private void transferPieceToTile(Tile tile) {
        this.tile.setPiece(null);
        tile.setPiece(piece);
        this.tile = tile;
    }
}
