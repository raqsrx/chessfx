package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class QueenPieceMover extends BasePieceMover {
    private int currentX;
    private int currentY;

    public QueenPieceMover(Tile tile) {
        super(tile);
    }

    @Override
    public List<Point> getMovablePoints() {
        this.currentX = this.piece.getLocation().x / BoardConstants.TILE_SIZE;
        this.currentY = this.piece.getLocation().y / BoardConstants.TILE_SIZE;
        List<Point> movablePoints = new ArrayList<>();

        movablePoints.addAll(getBishopMovablePoints());
        movablePoints.addAll(getRookMovablePoints());
        return movablePoints;
    }

    private List<Point> getRookMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        this.currentX = this.piece.getLocation().x / BoardConstants.TILE_SIZE;
        this.currentY = this.piece.getLocation().y / BoardConstants.TILE_SIZE;

        for (int y = getMinY(); y <= getMaxY(); y++) {
            movablePoints.add(new Point(currentX, y));
        }
        for (int x = getMinX(); x <= getMaxX(); x++) {
            movablePoints.add(new Point(x, currentY));
        }
        return movablePoints;
    }

    /**
     * Gets the max Y coordinate that the Rook can move to from its current Position
     * If a Piece from the same side exists, the last unoccupied Tile before the Piece is returned
     * If a Piece from the opposite side exists, the coordinates of the occupied Piece is returned
     *
     * @return - The max navigable Y coordinate
     */
    private int getMaxY() {
        for (int y = currentY + 1; y < BoardConstants.BOARD_HEIGHT; y++) {
            if (boardTiles[currentX][y].hasPiece()) {
                return isOppositeSide(boardTiles[currentX][y]) ? y : y - 1;
            }
        }
        return BoardConstants.BOARD_HEIGHT - 1;
    }

    /**
     * Gets the minimum Y coordinate that the Rook can move to
     * If a Piece from the same side exists, the unoccupied Tile before that is returned
     * If a Piece from the opposite side exists, the Tile occupied by the Piece is returned
     *
     * @return - The minimum navigable Y coordinate
     */
    private int getMinY() {
        for (int y = currentY - 1; y > -1; y--) {
            if (boardTiles[currentX][y].hasPiece()) {
                return isOppositeSide(boardTiles[currentX][y]) ? y : y + 1;
            }
        }
        return 0;
    }

    /**
     * Gets the max X coordinate that the Rook can move to from its current Position
     * If a Piece from the same side exists, the last unoccupied Tile before the Piece is returned
     * If a Piece from the opposite side exists, the coordinates of the occupied Piece is returned
     *
     * @return - The max navigable X coordinate
     */
    private int getMaxX() {
        for (int x = currentX + 1; x < BoardConstants.BOARD_WIDTH; x++) {
            if (boardTiles[x][currentY].hasPiece()) {
                return isOppositeSide(boardTiles[x][currentY]) ? x : x - 1;
            }
        }
        return BoardConstants.BOARD_WIDTH - 1;
    }

    /**
     * Gets the minimum X coordinate that the Rook can move to
     * If a Piece from the same side exists, the unoccupied Tile before that is returned
     * If a Piece from the opposite side exists, the Tile occupied by the Piece is returned
     *
     * @return - The minimum navigable X coordinate
     */
    private int getMinX() {
        for (int x = currentX - 1; x > -1; x--) {
            if (boardTiles[x][currentY].hasPiece()) {
                return isOppositeSide(boardTiles[x][currentY]) ? x : x + 1;
            }
        }
        return 0;
    }

    private List<Point> getBishopMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        this.currentX = piece.getLocation().x / BoardConstants.TILE_SIZE;
        this.currentY = piece.getLocation().y / BoardConstants.TILE_SIZE;

        Point rightDiagonalMaxPoint = getRightDiagonalMaxPoint();
        Point rightDiagonalMinPoint = getRightDiagonalMinPoint();
        Point leftDiagonalMaxPoint = getLeftDiagonalMaxPoint();
        Point leftDiagonalMinPoint = getLeftDiagonalMinPoint();

        for (int y = rightDiagonalMinPoint.y, x = rightDiagonalMinPoint.x;
             y <= rightDiagonalMaxPoint.y && x >= rightDiagonalMaxPoint.x;
             y++, x--
        ) {
            movablePoints.add(new Point(x, y));
        }

        for (int y = leftDiagonalMinPoint.y, x = leftDiagonalMinPoint.x;
             y <= leftDiagonalMaxPoint.y && x <= leftDiagonalMaxPoint.x;
             y++, x++
        ) {
            movablePoints.add(new Point(x, y));
        }

        return movablePoints;
    }

    /**
     * Gets the max left diagonal coordinates where the Bishop can move to from its current position
     * The left diagonal max point indicates the point with the highest Y value
     *
     * @return - Point containing the coordinates of the left diagonal max point
     */
    private Point getLeftDiagonalMaxPoint() {
        int y = currentY + 1;
        int x = currentX + 1;

        for (; y < BoardConstants.BOARD_HEIGHT && x < BoardConstants.BOARD_WIDTH; y++, x++) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }
        return new Point(x - 1, y - 1);
    }

    /**
     * Gets the left diagonal min point coordinates that the Bishop can move to
     * The left diagonal min point indicates the point with the lowest Y value
     *
     * @return - Point instance containing the left diagonal min coordinates
     */
    private Point getLeftDiagonalMinPoint() {
        int y = currentY - 1;
        int x = currentX - 1;

        for (; y > -1 && x > -1; y--, x--) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }

        return new Point(x + 1, y + 1);
    }

    /**
     * Gets the right diagonal max point coordinates
     * The right diagonal max point indicates the point in the right diagonal direction with the highest Y value
     *
     * @return - Point instance containing the right diagonal max point coordinates
     */
    private Point getRightDiagonalMaxPoint() {
        int y = currentY + 1;
        int x = currentX - 1;

        for (; y < BoardConstants.BOARD_HEIGHT && x > -1; y++, x--) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }
        return new Point(x + 1, y - 1);
    }

    /**
     * Gets the right diagonal min point coordinates
     * THe right diagonal min point indicates the point in the right diagonal with the lowest Y value
     *
     * @return - Point instance containing the right diagonal min point coordinates
     */
    private Point getRightDiagonalMinPoint() {
        int y = currentY - 1;
        int x = currentX + 1;

        for (; y > -1 && x < BoardConstants.BOARD_WIDTH; y--, x++) {
            if (!boardTiles[x][y].hasPiece()) {
                continue;
            }
            if (isOppositeSide(boardTiles[x][y])) {
                return new Point(x, y);
            }
            break;
        }
        return new Point(x - 1, y + 1);
    }
}
