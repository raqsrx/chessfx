package lk.raqeeb.chessfx.service.PieceMover;

import lk.raqeeb.chessfx.model.Side;
import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PawnPieceMover extends BasePieceMover {
    public PawnPieceMover(Tile tile) {
        super(tile);
    }

    /**
     * Gets the valid navigable Tiles that the Pawn can move to
     * Checks if the Pawn can capture Pieces that are placed diagonally
     *
     * @return - List of Point instances containing the coordinates of valid navigable Tiles
     */
    @Override
    public List<Point> getMovablePoints() {
        List<Point> movablePoints = new ArrayList<>();
        int oldX = piece.getLocation().x / BoardConstants.TILE_SIZE;
        int oldY = piece.getLocation().y / BoardConstants.TILE_SIZE;

        if (piece.getSide() == Side.WHITE) {
            // Cannot move out of the board dimensions
            if (oldY + 1 == BoardConstants.BOARD_HEIGHT) {
                return movablePoints;
            }

            // Move one or two depending on position
            if (!boardTiles[oldX][oldY + 1].hasPiece()) {
                movablePoints.add(new Point(oldX, oldY + 1));
                if (oldY == 1 && !boardTiles[oldX][oldY + 1].hasPiece()) {
                    movablePoints.add(new Point(oldX, oldY + 2));
                }
            }
            // Check if can move diagonally
            if (oldX - 1 > -1 && isOppositeSide(boardTiles[oldX - 1][oldY + 1])) {
                movablePoints.add(new Point(oldX - 1, oldY + 1));
            }
            if (oldX + 1 != BoardConstants.BOARD_WIDTH && isOppositeSide(boardTiles[oldX + 1][oldY + 1])) {
                movablePoints.add(new Point(oldX + 1, oldY + 1));
            }
        }
        if (piece.getSide() == Side.BLACK) {
            // Cannot move out of the board dimensions
            if (oldY - 1 < 0) {
                return movablePoints;
            }

            // Move one or two depending on current position
            if (!boardTiles[oldX][oldY - 1].hasPiece()) {
                movablePoints.add(new Point(oldX, oldY - 1));
                if (oldY == 6 && !boardTiles[oldX][oldY - 2].hasPiece()) {
                    movablePoints.add(new Point(oldX, oldY - 2));
                }
            }
            // Check if can move diagonally
            if (oldX + 1 != BoardConstants.BOARD_WIDTH && isOppositeSide(boardTiles[oldX + 1][oldY - 1])) {
                movablePoints.add(new Point(oldX + 1, oldY - 1));
            }
            if (oldX - 1 > -1 && isOppositeSide(boardTiles[oldX - 1][oldY - 1])) {
                movablePoints.add(new Point(oldX - 1, oldY - 1));
            }
        }
        return movablePoints;
    }
}
