package lk.raqeeb.chessfx.service.PiecePromoter;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lk.raqeeb.chessfx.model.PieceType;
import lk.raqeeb.chessfx.model.Side;
import lk.raqeeb.chessfx.servicefacade.PiecePromoter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FxPiecePromoter implements PiecePromoter {
    private Stage stage;
    private PieceType selectedType;

    public FxPiecePromoter(Side side) {
        try {
            this.stage = initScene(side);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * The target PieceType of the promoted Pawn
     * Display the window with the Prompt and wait till its closed to return the value
     *
     * @return - The target PieceType of the promoted Pawn
     */
    @Override
    public PieceType getTargetPieceType() {
        this.stage.showAndWait();
        return this.selectedType;
    }

    /**
     * Create new Window to allow user to choose the target PieceType for the promotion
     *
     * @param side - The Side of the Piece
     * @return - The created Stage instance which will be displayed for the prompt
     * @throws FileNotFoundException - Exception thrown if the Piece image is not available
     */
    private Stage initScene(Side side) throws FileNotFoundException {
        Stage stage = new Stage();
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10));
        vBox.setAlignment(Pos.CENTER);

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);

        Label label = new Label("Choose the type of Piece you want to promote to");
        label.setPadding(new Insets(0, 0, 10, 0));
        vBox.getChildren().add(label);

        for (PieceType type : PieceType.values()) {
            if (type == PieceType.PAWN || type == PieceType.KING) continue;
            Button button = new Button();
            button.setGraphic(new ImageView(new Image(
                    new FileInputStream(side == Side.WHITE ?
                            String.format(type.getImagePath(), "w") : String.format(type.getImagePath(), "b"))
            )));
            button.setOnMouseClicked(event -> {
                this.selectedType = type;
                stage.close();
            });
            hBox.getChildren().add(button);
        }
        vBox.getChildren().addAll(hBox);
        Scene scene = new Scene(vBox, 400, 200);
        stage.setScene(scene);
        return stage;
    }
}
