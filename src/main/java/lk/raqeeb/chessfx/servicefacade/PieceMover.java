package lk.raqeeb.chessfx.servicefacade;

import lk.raqeeb.chessfx.model.Piece;
import lk.raqeeb.chessfx.model.Tile;

import java.awt.*;
import java.util.List;

public interface PieceMover {
    /**
     * Sets the valid tiles the Piece can move to, so that they can be highlighted
     *
     * @param tiles - The list of valid tiles the piece can move to
     */
    void setMovableTiles(List<Point> tiles);

    /**
     * Get the list of valid points the piece can move to
     *
     * @return - List of Points where the piece can move to
     */
    List<Point> getMovablePoints();

    /**
     * Sets the tiles initialized in the Board class
     *
     * @param tiles - The 2D array containing the tiles
     */
    void setBoardTiles(Tile[][] tiles);

    /**
     * Moves the selected piece to the passed Tile instance
     *
     * @param tile - The Tile instance to which the Piece has to be set
     */
    void tryMove(Tile tile);

    /**
     * Captures / Kills the passed Piece instance
     *
     * @param piece - The Piece instance which will be captured
     */
    void tryKill(Piece piece);

    /**
     * Promote a Pawn to the selected target Piece type
     */
    void tryPromote();

    /**
     * Gets the Tile instances where there are Piece instances of the same Side
     *
     * @return - List of Point instances where there are Piece instances
     */
    List<Point> getTilesWithPieces();
}
