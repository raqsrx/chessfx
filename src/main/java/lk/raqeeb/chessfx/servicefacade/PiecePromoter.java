package lk.raqeeb.chessfx.servicefacade;

import lk.raqeeb.chessfx.model.PieceType;

public interface PiecePromoter {
    /**
     * Get the Target Piece type the Pawn is to be Promoted to
     *
     * @return - PieceType of the target promotion
     */
    PieceType getTargetPieceType();
}
