package lk.raqeeb.chessfx.model;

import lk.raqeeb.chessfx.service.PieceMover.*;
import lk.raqeeb.chessfx.util.ImageConstants;

public enum PieceType {
    KING(ImageConstants.KING, 4, 0, KingPieceMover.class.getName()),
    QUEEN(ImageConstants.QUEEN, 3, 0, QueenPieceMover.class.getName()),
    BISHOP(ImageConstants.BISHOP, 2, 0, BishopPieceMover.class.getName()),
    KNIGHT(ImageConstants.KNIGHT, 1, 0, KnightPieceMover.class.getName()),
    ROOK(ImageConstants.ROOK, 0, 0, RookPieceMover.class.getName()),
    PAWN(ImageConstants.PAWN, 1, 1, PawnPieceMover.class.getName());

    private String imagePath;
    private int x;
    private int y;
    private String pieceMover;

    PieceType(String imagePath, int defaultX, int defaultY, String pieceMover) {
        this.imagePath = imagePath;
        this.x = defaultX;
        this.y = defaultY;
        this.pieceMover = pieceMover;
    }

    public String getImagePath() {
        return this.imagePath;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public String getPieceMover() {
        return pieceMover;
    }
}
