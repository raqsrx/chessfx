package lk.raqeeb.chessfx.model;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import lk.raqeeb.chessfx.util.BoardConstants;

public class Tile extends Rectangle {
    private Piece piece;
    private boolean isSelected;
    private int xPos;
    private int yPos;

    public Tile(boolean isLight, int x, int y) {
        this.xPos = x;
        this.yPos = y;
        setFill(isLight ? Color.BEIGE : Color.DIMGREY);
        setWidth(BoardConstants.TILE_SIZE);
        setHeight(BoardConstants.TILE_SIZE);
        relocate(x * BoardConstants.TILE_SIZE, y * BoardConstants.TILE_SIZE);
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean hasPiece() {
        return piece != null;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void selectTile() {
        if (isSelected) {
            setStrokeWidth(0);
            isSelected = !isSelected;
            return;
        }
        setStrokeType(StrokeType.INSIDE);
        if (this.hasPiece()) {
            setStroke(Color.RED);
        } else {
            setStroke(Color.CYAN);
        }
        setStrokeWidth(5);
        isSelected = !isSelected;
    }

    public int getXPos() {
        return xPos;
    }

    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    public int getYPos() {
        return yPos;
    }

    public void setYPos(int yPos) {
        this.yPos = yPos;
    }
}
