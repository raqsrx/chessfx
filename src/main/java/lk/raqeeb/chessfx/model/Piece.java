package lk.raqeeb.chessfx.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import lk.raqeeb.chessfx.servicefacade.PieceMover;
import lk.raqeeb.chessfx.util.BoardConstants;
import lk.raqeeb.chessfx.util.ImageConstants;
import lk.raqeeb.chessfx.util.PieceUtils;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Piece extends StackPane {
    private Side side;
    private Point location;
    private boolean isAlive;
    private boolean isSelected;
    private Image image;
    private ImageView imageView;
    private PieceType type;
    private PieceMover pieceMover;

    public Piece(int x, int y) throws FileNotFoundException {
        boolean isWhite = y <= 1;
        this.side = isWhite ? Side.WHITE : Side.BLACK;
        this.setLocation(x, y);
        isAlive = true;
        this.type = PieceUtils.getPieceTypeByLocation(x, y);
        setImage(side == Side.WHITE ?
                String.format(type.getImagePath(), "w") : String.format(type.getImagePath(), "b")
        );
    }

    public void setLocation(int x, int y) {
        location = new Point(x * BoardConstants.TILE_SIZE, y * BoardConstants.TILE_SIZE);
        relocate(location.x, location.y);
    }

    public Side getSide() {
        return side;
    }

    public Point getLocation() {
        return location;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        if (!alive) {
            this.getChildren().remove(this.imageView);
        }
        isAlive = alive;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(String path) throws FileNotFoundException {
        if (this.imageView != null) {
            getChildren().remove(imageView);
        }
        this.image = new Image(new FileInputStream(path));
        this.imageView = new ImageView(image);
        this.imageView.setTranslateX(ImageConstants.TRANSLATE_X);
        this.imageView.setTranslateY(ImageConstants.TRANSLATE_Y);
        this.imageView.setFitHeight(ImageConstants.HEIGHT);
        this.imageView.setFitWidth(ImageConstants.WIDTH);
        getChildren().add(imageView);
    }

    public PieceType getType() {
        return type;
    }

    public void setType(PieceType type) {
        this.type = type;
        try {
            setImage(side == Side.WHITE ?
                    String.format(type.getImagePath(), "w") : String.format(type.getImagePath(), "b")
            );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ImageView getImageView() {
        return imageView;
    }

    public PieceMover getPieceMover() {
        return pieceMover;
    }

    public void setPieceMover(Tile tile) {
        try {
            this.pieceMover = (PieceMover) Class.forName(this.type.getPieceMover())
                    .getConstructor(Tile.class)
                    .newInstance(tile);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
