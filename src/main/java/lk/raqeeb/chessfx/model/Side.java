package lk.raqeeb.chessfx.model;

public enum Side {
    WHITE,
    BLACK
}
