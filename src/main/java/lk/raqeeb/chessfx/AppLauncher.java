package lk.raqeeb.chessfx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lk.raqeeb.chessfx.ui.board.Board;
import lk.raqeeb.chessfx.util.BoardConstants;

public class AppLauncher extends Application {
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(createContent());
        stage.setScene(scene);
        stage.show();
    }

    private Pane createContent() throws Exception {
        Pane root = new Pane();
        Board board = new Board();
        root.getChildren().add(board.getTileGroup());
        root.getChildren().add(board.getPieceGroup());
        root.setPrefSize(
                BoardConstants.BOARD_WIDTH * BoardConstants.TILE_SIZE, BoardConstants.BOARD_HEIGHT * BoardConstants.TILE_SIZE
        );
        return root;
    }

    public static void main(String[] args) {
        launch();
    }
}
