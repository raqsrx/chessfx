package lk.raqeeb.chessfx.ui.board;

import javafx.scene.Group;
import lk.raqeeb.chessfx.model.Piece;
import lk.raqeeb.chessfx.model.Tile;
import lk.raqeeb.chessfx.util.BoardConstants;

import java.util.Arrays;
import java.util.stream.Stream;

public class Board {
    private Group tileGroup;
    private Group pieceGroup;
    private Tile[][] tiles = new Tile[BoardConstants.BOARD_WIDTH][BoardConstants.BOARD_HEIGHT];

    public Board() throws Exception {
        tileGroup = new Group();
        pieceGroup = new Group();
        for (int y = 0; y < BoardConstants.BOARD_HEIGHT; y++) {
            for (int x = 0; x < BoardConstants.BOARD_WIDTH; x++) {
                Tile tile = new Tile((x + y) % 2 == 0, x, y);
                tileGroup.getChildren().add(tile);
                tiles[x][y] = tile;
                if (y > 1 && y < 6) continue;
                Piece piece = new Piece(x, y);
                tile.setPiece(piece);
                piece.setPieceMover(tile);
                pieceGroup.getChildren().add(piece);
            }
        }

        Stream.of(tiles)
                .flatMap(Arrays::stream)
                .filter(Tile::hasPiece)
                .map(Tile::getPiece)
                .map(Piece::getPieceMover)
                .forEach(mover -> mover.setBoardTiles(tiles));
    }

    public Group getTileGroup() {
        return tileGroup;
    }

    public Group getPieceGroup() {
        return pieceGroup;
    }
}
